embedded_components {
  id: "model"
  type: "model"
  data: "mesh: \"/dex/render/quads/quad.dae\"\n"
  "material: \"/dex/render/quads/quad.material\"\n"
  "skeleton: \"\"\n"
  "animations: \"\"\n"
  "default_animation: \"\"\n"
  "name: \"unnamed\"\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
