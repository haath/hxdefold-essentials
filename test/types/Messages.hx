package types;

import defold.types.Vector3;


@:build(defold.support.MessageBuilder.build())
class Messages
{
    var one: MessageOne;
    var two: Void;
}

typedef MessageOne =
{
    var value1: Float;
    var value2: Vector3;
}
