package types;

import defold.types.Message;
import defold.types.Url;
import defold.types.Vector3;
import dex.wrappers.DexScript;
import dex.wrappers.ScriptWithComponents.ScriptWithComponentsProperties;


typedef TestScriptProperties = ScriptWithComponentsProperties &
{
    @property var vec: Vector3;
    @property var bool: Bool;
    @property var value: Float;

    var localProperty: Float;
}

class TestScript extends DexScript<TestScriptProperties>
{
    override function init(self: TestScriptProperties)
    {
    }

    @msg("types.Messages.one")
    function msgOne(self: TestScriptProperties, value1: Float, value2: Vector3)
    {
        Sys.println('got one: $value1 $value2');
    }

    @msg("types.Messages.two")
    function msgTwo(self: TestScriptProperties)
    {
        Sys.println('got two');
    }

    @msg
    function msgDefaultHandler<T>(self: TestScriptProperties, messageId: Message<T>, message: T, sender: Url)
    {
        Sys.println('got default');
    }
}
