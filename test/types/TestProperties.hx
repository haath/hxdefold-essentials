package types;

import defold.types.Vector3;


@:build(dex.macro.PropertyBuilder.build())
class TestProperties
{
    var position: Vector3;
    var speed: Float;
    var alive: Bool;
    var positionX: Float = "position.x";
}
