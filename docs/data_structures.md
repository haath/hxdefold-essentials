
Data structures are under the `dex.util.ds` package.


## Matrix2D

This is a preallocated 2D array of fixed size.
It is an abstract over `Array<Array<T>>`, and exposes only the necessary methods to allow for read/write access to specific elements, while preventing changes to the size of the nested arrays.

Initialize with the dimensions, and the initial value for all elements.

```haxe
var matrix: Matrix2D<Int> = new Matrix2D(width, height, 0);
```

But it can also initialized from an existing 2D array.

```haxe
var matrix: Matrix2D<Int> = Matrix2D.fromArray([
    [ 1, 2, 3 ],
    [ 4, 5, 6 ],
    [ 7, 8, 9 ]
]);
```

Then interacting with the matrix is mainly done using array access.

```haxe
var elem: Int = matrix[x][y];

matrix[x][y] = 5;
```

Additionally, `map` and `forEach` are also supported.

```haxe
var matrixStrings: Matrix2D<String> = matrix.map(elem -> elem.toString());

matrixStrings.forEach(elem ->
{
    trace(elem);
});
```


## PriorityQueue

This is a typical priority queue.
It stores types that have a `priority` field, in ascending order of their priorities.

```haxe
typedef Item =
{
    var priority: Int;
}

var pq: PriorityQueue<Item> = new PriorityQueue();
pq.add({ priority: 5 });
pq.add({ priority: 7 });
pq.add({ priority: 2 });
pq.add({ priority: 8 });

while (pq.length > 0)
{
    var item: Item = pq.getNext();

    trace(item.priority);
}

// will print: 2, 5, 7, 8
```

