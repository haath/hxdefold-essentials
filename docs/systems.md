
Several top-level system utilities are available. To enable them add a script to the scene that extends `SystemManager`.

```haxe
import dex.systems.scripts.SystemManager;

typedef GameManagerProperties = SystemManagerProperties &
{
}

class GameManager extends SystemManager<GameManagerProperties>
{
}
```

All systems are implemented as global static classes.


## Time system

The `Time` system is a utility for timing events.

```haxe
// number unique to the current frame
// will loop around every 2^32 frames
var currentFrameNumber: UInt = Time.frame;

// the time (in seconds) that elapsed since init() was called
var currentTime: Float = Time.timestamp;
```

```haxe
if (Time.every(0.5))
{
    // this branch will be taken once every 0.5sec
}
if (Time.everyFrames(10))
{
    // this branch will be taken once every 10 frames
}
if (Time.everyFrames(10, 2))
{
    // this branch will be taken once every 10 frames,
    // but always two frames away than the branch above
}
```

### Watchdogs

Watchdogs are callbacks which are invoked after a specified delay, unless they are reset.

For example, suppose that if the player gives no input for 5 seconds you want to change the animation of the character to one where he looks bored.

```haxe
var wd: Watchdog = Time.watchdog(5.0, true, () -> { player.setBored(); });
```

Then whenever there is input from the player, reset the watchdog to prevent it from triggering.

```haxe
function onPlayerInput()
{
    wd.reset();
}
```

Watchdogs can be repeating or non-repeating, according to the second argument to the `Time.watchdog` function. To stop a watchdog use the `wd.stop()` method.

Note that, the reference to the returned `Watchdog` object must be immediatelly discarded after it has been stopped. This applies both to a repeating watchdog that had `stop()` called, and to a non-repeating watchdog that has been tripped.


## Metrics system

The `Metrics` system has several methods for obtaining performance metrics about the game at runtime.

- `Metrics.getFps()`: get the average FPS
- `Metrics.getFrameTime()`: get the average frame time in seconds
- `Metrics.getMemoryUsage()`: get the current memory usage of the game in KB
- `Metrics.getMemoryUsageFormatted()`: get the memory usage as a human-readable string

There are also some methods for obtaining the peak values: `getMinFps()`, `getPeakFrameTime()`, `getPeakMemoryUsage()`, and `getPeakMemoryUsageFormatted()`.

The peak counters need to be reset periodically. This can be done by calling `Metrics.resetPeakCounters()`, or by configuring the property `SystemManagerProperties.resetMetricsPeaksEvery` of the `SystemManager`.


## Postman system

The `Postman` is a simple system that allows all objects to subscribe to certain message types, and then other objects to send messages that reach all subscribers.

Objects who are interested to receive messages of a type may use `Postman.subscribe()`.

```haxe
// the object calling this method will receive all some_message_id messages
Postman.subscribe("some_message_id");

// other objects can also be subscribed to messages
Postman.subscribe("some_message_id", anotherObject);
```

Then any script that wishes to broadcast a message to all others should call `Postman.send()`.

```haxe
// all subscribers to some_message_id will then receive it
Postman.send("some_message_id");
```

