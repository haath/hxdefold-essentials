
## Rng

The `Rng` class is a zero-overhead implementation of a seed pseudorandom number generator.

```haxe
// initialize a generator based on a seed
var rng: Rng = new Rng(12345);

// initialize a generator with a time-dependant seed
var rng: Rng = new Rng();
```

The generator then has various methods for generating data, some of which are: `int(min ,max)`, `bool(chance)`, `pick(array)`, `pickWeighted(array, weights)`, `normal(mean, std)`, `shuffle(array)`.

Furthermore, there are two ways to use the generator to create other seeded-generators.

- The `rng.rng()` method, which returns a new `Rng` instance.
- The `rng.seed()` method which generates a `UInt` seed, which can be used to seed another generator.


## TrickDie

This is a utility class which represents a *fraudulent* dice-roll generator.
The dice rolls from this generator will be forced to go through all possible sides of the die over a configurable number of rolls.

This is useful in cases where cases of extreme good or bad luck should be avoided.

The result can be tuned using the `period` parameter. For example, if rolling `D4` dice:

- A `period` of `1` means that the numbers `[1, 2, 3, 4]` will each be rolled at least once in the first `4` rolls, then again in the next `8` etc.
- A `period` of `2` means that the numbers `[1, 2, 3, 4]` will each be rolled at least twice in the first `8` rolls, then again in the next `8` etc.
- and so on...

So with increasing `period` values, the random sequence will become more unpredictable, but also extreme cases will become more likely.

```haxe
// create a new generator of D6 dice rolls, with a period of 2 and no seed
var dieGen: TrickDie = new TrickDie(D6, 2);

// get die rolls
// since we picked D6, this will return a value between 1 and 6
var roll: UInt = dieGen.roll();
```
