
Extend the class `DexScript` or `DexScriptWithComponents` to get access to the wrapper functionality for scripts.

Limitations:

- These scripts can't define their own `on_message`. Instead, a default callback should be specified as a catch-all for all messages that don't match any of the other callbacks.


## Message handlers

Scripts that extend `DexScript` don't get to define an `on_message` method.
Instead, they define methods for each message type that they wish to handle.

The expressions given as string arguments to `@msg` will be used exactly as they are in the switch `case ...` statements.
Therefore, in the context of this class, they need to resolve to an actual `Message<T>` type.

```haxe
import dex.wrappers.DexScript;

typedef TestScriptProperties =
{
    @property var vec: Vector3;
    @property var bool: Bool;
    @property var value: Float;

    var localProperty: Float;
}

class TestScript extends DexScript<TestScriptProperties>
{
    @msg("types.Messages.one") function msgOne(value1: Float, value2: Vector3)
    {
        // the type types.Messages.one is a type with two fields with the same name and order
        // typedef MessageOne =
        // {
        //     var value1: Float;
        //     var value2: Vector3;
        // }
    }

    @msg("types.Messages.two") function msgTwo()
    {
        // this message handler because the message type is Void
    }

    @msg function defaultHandler<TMessage>(self: TestScriptProperties, messageId: Message<TMessage>, message: TMessage, sender: Url)
    {
        // this is the default case that will get called for messages that don't match any of the other callbacks
    }
}
```

Note that comparing to the traditional message handling, this has the same type-safety but not the same compiler assistance.
Because these method calls are macro-generated, the error messages may end up a bit cryptic, depending on the error.
