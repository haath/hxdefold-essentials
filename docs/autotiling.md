

```haxe
var tiledMap: TiledMap = Tiled.parse(Resource.getString('map-untitled'));
var tileset: TiledTileset = Tiled.parseTileset(Resource.getString('tileset'));
var layer: TiledMapTileLayer = tiledMap[ 'ground' ];

var bitmasks: Map<UInt, TileBitmask> = tileset.getWangSetWithName('walls').getTileBitmasks();
bitmasks.set(367, TileBitmask.center);

var autotiler: Autotitler = new Autotitler(bitmasks);

var autotiled: Matrix2D<UInt> = autotiler.autotile(layer.toMatrix());
```
