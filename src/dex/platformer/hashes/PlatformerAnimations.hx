package dex.platformer.hashes;

@:build(defold.support.HashBuilder.build())
class PlatformerAnimations
{
    var idle;
    var run;
    var walk;
    var walk_backwards;
    var jump;
    var roll;
    var climb;
    var fall;
    var crouch;
    var land;
}
