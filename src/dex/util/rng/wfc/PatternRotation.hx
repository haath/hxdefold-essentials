package dex.util.rng.wfc;

enum abstract PatternRotation(Int)
{
    var Clockwise;
    var Semicircle;
    var CounterClockwise;
}
