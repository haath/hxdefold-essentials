package dex.types;

enum abstract CameraRecoil(Int)
{
    var SineDiminishing;
    var Square;
    var SquareDiminishing;
    var OutIn;
}
