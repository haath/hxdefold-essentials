package dex.tiled;

enum abstract TiledLayerType(String)
{
    var TileLayer = 'tilelayer';
    var ObjectLayer = 'objectgroup';
}
