package dex.tiled.infinite;

typedef InfiniteTiledMap =
{
    var layers: Array<InfiniteTiledMapLayer>;
}
