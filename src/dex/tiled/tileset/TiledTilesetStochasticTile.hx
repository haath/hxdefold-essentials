package dex.tiled.tileset;

typedef TiledTilesetStochasticTile =
{
    var id: Int;
    var probability: Float;
}
