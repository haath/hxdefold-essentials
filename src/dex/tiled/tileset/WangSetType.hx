package dex.tiled.tileset;

enum abstract WangSetType(String)
{
    var Edge = 'edge';
    var Corner = 'corner';
    var Mixed = 'mixed';
}
